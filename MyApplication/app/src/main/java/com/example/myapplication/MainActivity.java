package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    TextView text, textylä;
    EditText sentence;
    EditText fileName;
    Context context = null;
    Button button;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.testFunction();
        button = (Button) findViewById(R.id.button3);
        text = (TextView) findViewById(R.id.textView);
        textylä = (TextView) findViewById(R.id.tekstiylä);
        fileName = (EditText) findViewById(R.id.editTextNew);
        context = MainActivity.this;
        sentence = (EditText) findViewById(R.id.editText2);
        System.out.println("Sijainti" + context.getFilesDir());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text.setText(sentence.getText().toString());
                System.out.println("Hello world!!");
            }
        });
        sentence.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                textylä.setText(s);
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        }
        );
    }
    public void readFile(View v){
        try {
            String filenameInput;
            filenameInput = fileName.getText().toString();
            InputStream ins = context.openFileInput(filenameInput);
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s ="";


            while((s=br.readLine()) !=null){
                text.setText(s);
            }
            ins.close();
        } catch (IOException e){
            Log.e("IOException", "Virhe syötteessä");
        }finally{
            System.out.println("Luettu");
        }


    }

    public void writeFile(View v) throws IOException {
        String filenameInput2;
        filenameInput2 = fileName.getText().toString();
        OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(filenameInput2, context.MODE_PRIVATE));
        String sentenceInput;
        sentenceInput = sentence.getText().toString();
        ows.write(sentenceInput);

        ows.close();
    }


}
