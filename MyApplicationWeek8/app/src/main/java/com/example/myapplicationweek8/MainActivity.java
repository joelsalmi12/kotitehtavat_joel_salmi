package com.example.myapplicationweek8;



import android.content.Context;
import android.os.Bundle;

import android.view.View;
import android.view.textservice.TextInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static SeekBar seek_bar;
    private static TextView text_Money;
    TextView text;
    Spinner spinner;
    Spinner spinner_Size;
    TextView text_Info;
    BottleDispenser bd;
    Button button2;
    Button buttonbuyBottle;
    Context context = null;
    final ArrayList<Bottle> bottleList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.buttonPrice);
        buttonbuyBottle = findViewById(R.id.buttonbuyBottle);
        Button button3 = findViewById(R.id.button3);
        bd = BottleDispenser.getInstance();
        text = (TextView) findViewById(R.id.textView);
        text_Info = (TextView) findViewById(R.id.textViewInfo);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner_Size =(Spinner) findViewById(R.id.spinner2);
        context = MainActivity.this;

        Bottle pepsi = new Bottle();
        bottleList.add(pepsi);
        Bottle zero = new Bottle("Coca-Cola Zero", "0.5", 2.0f);
        bottleList.add(zero);
        Bottle fanta = new Bottle("Fanta Zero", "0.5", 1.95f);
        bottleList.add(fanta);
        Bottle bigFanta = new Bottle("Coca-Cola Zero", "1.5", 2.5f);
        bottleList.add(bigFanta);
        Bottle bigPepsi = new Bottle("Pepsi Max", "1.5", 2.2f);
        bottleList.add(bigPepsi);
        bottleCreater();
        buyBottle();
        System.out.println("Sijainti"+ context.getFilesDir());
        seek_bar = (SeekBar) findViewById(R.id.seek_bar);
        text_Money = (TextView) findViewById(R.id.textViewMoney);
        text_Money.setText("Covered : " + seek_bar.getProgress());
        seek_bar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress_value;

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progress_value = progress;
                        text_Money.setText("Add : " + progress + "€");
                        button1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                bd.addMoney(progress_value);
                                text.setText("You added " + progress_value);
                                seek_bar.setProgress(0);
                            }
                        });
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                }
        );
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bd.returnMoney() > 0) {
                    DecimalFormat mr = new DecimalFormat("0.00");
                    text.setText("Klink klink. Money came out! You got " + mr.format(bd.returnMoney()) + "€ back");
                    bd.clearAmount();
                } else {
                    text.setText("You haven´t money left");

                }
            }
        });

    }
    private void bottleCreater() {


        ArrayList<String> bottlelist = new ArrayList<>(); // Created to show Bottles in spinner
        bottlelist.add("Pepsi Max");
        bottlelist.add("Coca-Cola Zero");
        bottlelist.add("Fanta Zero");

        ArrayList<String> bottlelistSize = new ArrayList<String>(); // Created to show size of the bottles in spinner
        bottlelistSize.add("0.5");
        bottlelistSize.add("1.5");


        //FIRST SPINNER WHICH IS SHOWS NAME OF BOTTLE
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, bottlelist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // LETS MAKE ANOTHER SPINNER WHICH SHOWS SIZE OF THE BOTTLE
        ArrayAdapter<String> adapter2 =
                new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, bottlelistSize);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Size.setAdapter(adapter2);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = spinner.getSelectedItem().toString();
                String size = spinner_Size.getSelectedItem().toString();
                Integer count = 0;
                for (Bottle A : bottleList){
                    System.out.println(A.getName() + " "+ A.getPrice());
                    if ((name.equals(A.getName())) && (size.equals(A.getSize()))){
                        text_Info.setText("Price: "+ A.getPrice());
                        count +=1;
                    }


                }if (count==0) text_Info.setText("Not available, please select again.");
            }
        });

    }
    public void buyBottle() {
        buttonbuyBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = spinner.getSelectedItem().toString();
                String size = spinner_Size.getSelectedItem().toString();
                for (Bottle A : bottleList){
                    if ((name.equals(A.getName())) && (size.equals(A.getSize()))){
                        if (bd.returnMoney() >= A.getPrice()) {
                            bd.setMoney(A.getPrice());
                            Float price = A.getPrice();
                            text.setText("Kalunk! " + A.getName() + " came out of the dispenser. Enjoy!");

                            try {
                                writeFile(price);
                                System.out.println("Avasi!");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        else if ((bd.returnMoney()< A.getPrice())){
                            DecimalFormat mr = new DecimalFormat("0.00");
                            text.setText("Please, add " + mr.format(A.getPrice()-bd.returnMoney())+" € more!");

                        }
                    }



                }}
        });

    }
    public void writeFile(Float price) throws IOException {
        String name = spinner.getSelectedItem().toString();
        String size = spinner_Size.getSelectedItem().toString();
        OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("Kuitti.txt", context.MODE_PRIVATE));
        ows.write("###Hey! You just used bottledispenser-app! Here is the receipt. Enjoy your day###\n \nProduct: " + name + "\nPrice: "+ price+ "€\n \n SS-Solutions Oy");
        ows.close();
    }

}