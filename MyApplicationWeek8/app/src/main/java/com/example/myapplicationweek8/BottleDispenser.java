package com.example.myapplicationweek8;



public class BottleDispenser {
    private double money =0;
    private static BottleDispenser bd = new BottleDispenser();
    public static BottleDispenser getInstance() {
        return bd;
    }

    private BottleDispenser() {

        money = 0;
    }

    public void addMoney(double x) {
        money += x;
    }
    public void setMoney(float a){money -= a;}
    public Double returnMoney() {
        return money;
    }
    public void clearAmount(){
        money=0;
    }

}