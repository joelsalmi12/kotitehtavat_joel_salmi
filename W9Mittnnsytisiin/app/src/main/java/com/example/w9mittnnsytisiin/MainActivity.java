package com.example.w9mittnnsytisiin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;



    public class MainActivity extends AppCompatActivity {
        Spinner main_spinner;
        Spinner spinner_Finland;
        Button button;
        smartpostClass sm;
        Button button_Info;
        TextView text;
        Button button_FIN;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            spinner_Finland = (Spinner) findViewById(R.id.spinner_Finland);
            main_spinner = (Spinner) findViewById(R.id.spinner);
            button_Info = (Button) findViewById(R.id.buttonInfo);
            button_FIN = (Button) findViewById(R.id.buttonFIN);
            button = (Button) findViewById(R.id.button1);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            sm = smartpostClass.getInstance();
            text = (TextView) findViewById(R.id.textView);
            final ArrayList<smartpostObj> arr = sm.myList();
            final ArrayList<smartpostObj> arrFIN = sm.myListFIN();
            try {
                sm.readXML();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
            ArrayAdapter<smartpostObj> adapter =
                    new ArrayAdapter<smartpostObj>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arr);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            main_spinner.setAdapter(adapter);
            ArrayAdapter<smartpostObj> adapter1 =
                    new ArrayAdapter<smartpostObj>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrFIN);
            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_Finland.setAdapter(adapter1);
            button_Info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = main_spinner.getSelectedItem().toString();
                    for (smartpostObj A : arr) {
                        if (A.name.equals(name) == true) {
                            text.setText("Open \n" + A.getDate() + "\n"+ A.getAddress() + "\n" + A.getLocation());
                        }
                    }


                }

            });
            button_FIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String nameFIN = spinner_Finland.getSelectedItem().toString();
                    for (smartpostObj B : arrFIN){
                        if (B.name.equals(nameFIN) == true) {
                            text.setText("Open \n" + B.getDate() + "\n"+ B.getAddress() + "\n" + B.getLocation());
                        }
                    }
                }
            });




        }





}
