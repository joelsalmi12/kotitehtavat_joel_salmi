package com.example.w9mittnnsytisiin;

import android.widget.ArrayAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class smartpostClass {
    ArrayList <smartpostObj> smp_list = new ArrayList<>();
    ArrayList <smartpostObj> smp_listFIN = new ArrayList<>();
    private static smartpostClass sm = new smartpostClass();
    public static smartpostClass getInstance() {
        return sm;
    }

    private smartpostClass() {


    }
    public void readXML() throws ParserConfigurationException {
        try {

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlStringEE = "http://iseteenindus.smartpost.ee/api/?request=destinations&country=EE&type=APT";
            String urlStringFIN = "http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT";
            Document doc2 = builder.parse(urlStringFIN);
            Document doc = builder.parse(urlStringEE);

            System.out.println("Root element "+ doc.getDocumentElement().getNodeName());
            System.out.println("Root element " + doc2.getDocumentElement().getNodeName());
            NodeList nList =doc.getDocumentElement().getElementsByTagName("item");
            NodeList n2List = doc2.getDocumentElement().getElementsByTagName("item");
            System.out.println("BYE BYE, I´M GOING IN! SEE YOU SOON!");
            for (int i =0; i<nList.getLength() ; i++){
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    System.out.println("Heyy I´m in here");
                    String location = element.getElementsByTagName("city").item(0).getTextContent();
                    String avail1 = element.getElementsByTagName("availability").item(0).getTextContent();
                    String address = element.getElementsByTagName("address").item(0).getTextContent();
                    String nameEE = element.getElementsByTagName("name").item(0).getTextContent();
                    smartpostObj temp = new smartpostObj(location, avail1, address, nameEE);
                    smp_list.add(temp);
                }
            }
            for (int i =0; i<n2List.getLength() ; i++){
                Node node = n2List.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element element = (Element) node;
                    System.out.println("Heyy I´m in here");
                    String locationFIN = element.getElementsByTagName("city").item(0).getTextContent();
                    String availFIN = element.getElementsByTagName("availability").item(0).getTextContent();
                    String addressFIN = element.getElementsByTagName("address").item(0).getTextContent();
                    String nameFIN = element.getElementsByTagName("name").item(0).getTextContent();
                    smartpostObj temp2 = new smartpostObj(locationFIN,availFIN, addressFIN, nameFIN);
                    smp_listFIN.add(temp2);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        finally{
            System.out.println("############## HEY ##############");
        }


    }
    public ArrayList<smartpostObj> myList()    {
        return(smp_list);
    }
    public ArrayList<smartpostObj> myListFIN()    {
        return(smp_listFIN);
    }

}
