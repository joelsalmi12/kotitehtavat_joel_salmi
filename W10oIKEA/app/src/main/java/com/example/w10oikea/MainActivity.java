package com.example.w10oikea;


import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {
        int index = 0;
        int index2 = 0;
        int counter =0;
        WebView web;
        EditText searchPlank;
        Button searchButton;
        Button refreshButton;
        Button initializeButton;
        Button shoutoutButton;
        Button previousButton;
        Button nextButton;
        String url;
       final ArrayList<String> urlArrayHistory = new ArrayList<String>();
       final ArrayList <String> urlArrayNext = new ArrayList<String>();
        final ArrayList <String> urlArrayActive = new ArrayList<String>();



        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            web = findViewById(R.id.WebView1);
            refreshButton = (Button) findViewById(R.id.buttonRefresh);
            searchPlank = (EditText) findViewById(R.id.editText);
            searchButton = (Button) findViewById(R.id.searchButton);
            shoutoutButton = (Button) findViewById(R.id.shoutOut);
            initializeButton = (Button) findViewById(R.id.initializeButton);
            previousButton = (Button) findViewById(R.id.previousButton);
            nextButton = (Button) findViewById(R.id.nextButton);
            web.setWebViewClient(new WebViewClient());
            web.getSettings().setJavaScriptEnabled(true);
            addUrl();
            refreshButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    web.reload();
                }
            });

            previousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (urlArrayHistory.size()>0) {
                        ListIterator<String> itr2 = urlArrayHistory.listIterator(urlArrayHistory.size());
                        addNext();
                        String var2 = itr2.previous();
                        if (var2.equals("index.html")){activateIndex();}
                        else{ web.loadUrl("http://" + var2);}
                        urlArrayActive.add(var2);
                        urlArrayHistory.remove(var2);

                    } else{
                        System.out.println("No more");
                }}
            });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (urlArrayNext.size()>0) {
                    ListIterator<String> itr4 = urlArrayNext.listIterator(urlArrayNext.size());
                    addHistoryNext();
                    String var4 = itr4.previous();
                    if (var4.equals("index.html")){activateIndex();}
                    else{ web.loadUrl("http://" + var4);}
                    urlArrayActive.add(var4);
                    urlArrayNext.remove(var4);
                }else{
                    System.out.println("No more");}
            }
        });
        }




    public void addUrl(){
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                url = String.valueOf(searchPlank.getText());
                if (url.equals("index.html")) {
                    activateIndex();
                    urlArrayActive.add(url);
                    addHistory();
                    checkNext();
                }
                else {

                    web.loadUrl("http://" + url);
                    urlArrayActive.add(url);
                    addHistory();
                    checkNext();
                }

            }
        });

        }
            public void activateIndex(){
                web.loadUrl("file:///android_asset/index.html");
                shoutoutButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("Lähellä ollaan !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            web.evaluateJavascript("javascript:shoutOut()",null );
                        }
                    }
                });
                initializeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            web.evaluateJavascript("javascript:initialize()", null);
                        }
                    }
                });
            }
           public void addHistory() {
               if (urlArrayActive.size()>1){
                   ListIterator<String> itr = urlArrayActive.listIterator(urlArrayActive.size()-1);
                   String var = itr.previous();
                   urlArrayHistory.add(var);
                   urlArrayActive.remove(var);
               }
           }
           public void addNext(){
               ListIterator<String> itr3 = urlArrayActive.listIterator(urlArrayActive.size());
                String var3 = itr3.previous();
                urlArrayNext.add(var3);
                urlArrayActive.remove(var3);
           }
           public void addHistoryNext(){
               ListIterator<String> itr5 = urlArrayActive.listIterator(urlArrayActive.size());
               String var6 = itr5.previous();
               urlArrayHistory.add(var6);
               urlArrayActive.remove(var6);

           }
           public void checkNext(){
            if (urlArrayNext.size()>0){
                for (String A : urlArrayNext){
                    urlArrayHistory.add(A);
                    urlArrayNext.remove(A);
                }
            }
           }




}

